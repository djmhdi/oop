package week2;

public abstract class BinaryExpression extends Expression {
    abstract public Expression left();
    abstract public Expression right();
}
