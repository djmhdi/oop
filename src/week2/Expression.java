package week2;

abstract public class Expression {
    abstract public String toString();
    abstract public int evaluate();
}
